directory "/srv/www/yb/current/wp-content" do
  owner "deploy"
  group "apache"
  mode 00755
end
file "/srv/www/yb/current/.htaccess" do
  owner "deploy"
  group "apache"
  mode 00644
end
file "/srv/www/yb/current/wp-config.php" do
  owner "deploy"
  group "apache"
  mode 00644
end
%w[ "/srv/www/yb/current/wp-content/**/*" ].each do |path|
  file path do
   mode 00665
  end if File.file?(path)
  directory path do
   mode 00775
   end if File.directory?(path)
end

directory "/srv/www/isomonline_beta/current" do
  owner "deploy"
  group "apache"
  mode 00775
end
%w[ "/srv/www/isomonline_beta/current/**/*" ].each do |path|
  file path do
   mode 00665
  end if File.file?(path)
  directory path do
   mode 00775
  end if File.directory?(path)
end
group "apache" do
  action :modify
  members "ec2-user"
  append true
end