cron "delete_error_log" do
  hour "12"
  minute "30"
  command 'sudo find /srv/www/ -name "error_log" -delete'
  action :create
end